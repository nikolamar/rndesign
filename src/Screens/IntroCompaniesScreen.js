// @flow weak

import React from 'react'
import { ScrollView, View, Text } from 'react-native'
import Card from '../Views/Card'
import { shared } from '../Styles'
import Nav from '../Views/Nav'
import Grid from '../Views/Grid'
import Title from '../Views/Title'
import Section from '../Views/Section'
import Line from '../Views/Line'
import db from '../JSON/db'

const renderCompany = item => (
  <Card label={item.company}/>
)

const CompaniesScreen = () => (
  <View style={shared.container}>
    <Nav rightLabel="Skip"/>
    <Grid
      itemWidth={100}
      itemHeight={100}
      marginVertical={10}
      data={db.companies}
      renderItem={renderCompany}
    />
    <Title>
      Companies
    </Title>
    <Section>
      Discover the best jobs in London search companies that are hiring.
    </Section>
  </View>
)

export default CompaniesScreen