// @flow weak

import React from 'react'
import { TouchableOpacity, ScrollView, View, Text } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'
import { MapView } from 'expo'
import Grid from '../Views/Grid'
import ListItem from '../Views/ListItem'
import { shared, colors } from '../Styles/'
import Line from '../Views/Line'
import SimilarJobs from '../Views/SimilarJobs'
import JobFooter from '../Views/JobFooter'
import JobLocation from '../Views/JobLocation'
import JobDetails from '../Views/JobDetails'
import AboutJob from '../Views/AboutJob'
import Message from '../Views/Message'
import Slider from '../Views/Slider'
import Button from '../Views/Button'
import Toolbar from '../Views/Toolbar'
import db from '../JSON/db'
import { resetAction } from '../Actions'

const JobScreen = ({ navigation }) => (
  <View style={{marginTop: 20}}>
    <Toolbar navigation={navigation}/>
    <ScrollView>
      <AboutJob style={{marginHorizontal: 20}}/>
      <Line style={{marginVertical: 40, marginHorizontal: 20}}/>
      <Slider style={{marginHorizontal: 20, height: 186}}/>
      <Line style={{marginVertical: 40, marginHorizontal: 20}}/>
      <Message
        style={{marginHorizontal: 20, marginBottom: 10}}
        primary="Summary. "
        secondary="Here’s what you need to know. ⚡"
      />
      <JobDetails
        category={db.job.category}
        pay={db.job.pay}
        required={db.job.required}
        skills={db.job.skills}
        style={{
          marginHorizontal: 20
        }}
      />
      <JobLocation
        address={db.job.address}
        mapStyle={{height: 200}}
        region={{
          latitude: db.job.latitude,
          longitude: db.job.longitude,
          latitudeDelta: 0.005,
          longitudeDelta: 0.005,
        }}
      />
      <SimilarJobs
        style={{
          marginHorizontal: 20,
          marginVertical: 50,
        }}
      />
      <Button
        color={colors.primary}
        style={{marginVertical: 5, marginHorizontal: 20}}
        onPress={() => navigation.dispatch(resetAction("WalkthroughRoute"))}
      >
        Walkthrough
      </Button>
      <Button
        color={colors.primary}
        style={{marginBottom: 50, marginHorizontal: 20}}
        onPress={() => navigation.dispatch(resetAction("HomeRoute"))}
      >
        Home
      </Button>
      <JobFooter/>
    </ScrollView>
  </View>
)

export default JobScreen