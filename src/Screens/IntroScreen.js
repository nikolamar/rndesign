// @flow weak

import React, { Component } from 'react'
import { ScrollView, View, Text, Image } from 'react-native'
import Nav from '../Views/Nav'
import Grid from '../Views/Grid'
import Title from '../Views/Title'
import Section from '../Views/Section'
import { colors, shared } from '../Styles/'

const CompaniesScreen = () => (
  <View style={shared.container}>
    <Nav rightLabel="Skip"/>
    <Title>
      We help great people find the best hospitality jobs in London.
    </Title>
    <Section style={{ marginBottom: 5 }}>
      inploi is a community-driven marketplace, enabling people to discover and apply for hospitality jobs with London’s best employers - online or from a mobile phone.
    </Section>
    <View style={{ flex: 1, marginBottom: 20, marginTop: 10 }}>
      <View style={{ flex: 1, flexDirection: "row" }}>
        <Image
          style={shared.image}
          resizMode="contain"
          source={require('../../assets/images/bar.png')}
        >
          <Text style={shared.imageTitle}>Barista</Text>
        </Image>
        <Image
          style={shared.image}
          resizMode="contain"
          source={require('../../assets/images/barista.png')}
        >
          <Text style={shared.imageTitle}>Waiting</Text>
        </Image>
      </View>
      <View style={{ flex: 1, flexDirection: "row" }}>
        <Image
          style={shared.image}
          resizMode="contain"
          source={require('../../assets/images/chef.png')}
        >
          <Text style={shared.imageTitle}>Bar</Text>
        </Image>
        <Image
          style={shared.image}
          resizMode="contain"
          source={require('../../assets/images/waiting.png')}
        >
          <Text style={shared.imageTitle}>Chef</Text>
        </Image>
      </View>
    </View>
  </View>
)

export default CompaniesScreen