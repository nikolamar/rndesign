// @flow weak

import React, { Component } from 'react'
import { ScrollView, View, Text, TextInput } from 'react-native'
import { shared, colors } from '../Styles/'
import ListItem from '../Views/ListItem'
import Nav from '../Views/Nav'
import Line from '../Views/Line'
import Grid from '../Views/Grid'
import Title from '../Views/Title'
import DescriptiveCard from '../Views/DescriptiveCard'
import RecentlyAdded from '../Views/RecentlyAdded'
import Message from '../Views/Message'
import { resetAction } from '../Actions'
import db from '../JSON/db'

class JobsScreen extends Component {
  renderFeaturedJob = item => (
    <DescriptiveCard
      itemCardWidth={280}
      itemCardHeight={155}
      head={item.wage}
      title={item.title}
      titleColor={colors.primary3}
      subtitle={item.description}
      onPress={() => this.props.navigation.dispatch(resetAction("JobRoute"))}
    />
  )
  renderRecomendedJob = item => (
    <DescriptiveCard
      head={item.wage}
      title={item.title}
      titleColor={colors.primary2}
      subtitle={item.company}
      onPress={() => this.props.navigation.dispatch(resetAction("JobRoute"))}
    />
  )
  render() {
    return (
      <ScrollView style={shared.container}>
        <Message
          style={{marginBottom: 30}}
          primary="Featured. "
          secondary="The hottest, freshest jobs around. 😎"
        />
        <Grid
          horizontal
          itemWidth={300}
          itemHeight={224}
          data={db.featuredJobs}
          renderItem={this.renderFeaturedJob}
        />
        <Line style={{marginVertical: 30}}/>
        <Message
          style={{marginBottom: 30}}
          primary="For you. "
          secondary="Recommended by us. 🔬"
        />
        <Grid
          horizontal
          itemWidth={145}
          itemHeight={169}
          data={db.recomendedJobs}
          renderItem={this.renderRecomendedJob}
        />
        <Line style={{marginVertical: 30}}/>
        <Nav
          style={{marginBottom: 30}}
          leftStyle={shared.bold}
          rightStyle={{ color: colors.primary7 }}
          leftLabel="Recently Added"
          rightLabel="See All"
        />
        <RecentlyAdded data={db.recentlyAdded}/>
      </ScrollView>
    )
  }
}

export default JobsScreen