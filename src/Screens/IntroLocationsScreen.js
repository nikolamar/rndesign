// @flow weak

import React from 'react'
import { View } from 'react-native'
import Card from '../Views/Card'
import { shared } from '../Styles'
import Nav from '../Views/Nav'
import Grid from '../Views/Grid'
import Title from '../Views/Title'
import Section from '../Views/Section'
import db from '../JSON/db'

const renderCity = item => (
  <Card label={item.city}/>
)

const IntroLocationsScreen = () => (
  <View style={shared.container}>
    <Nav rightLabel="Skip"/>
    <Grid
      itemWidth={100}
      itemHeight={100}
      marginVertical={10}
      data={db.cities}
      renderItem={renderCity}
    />
    <Title>
      Locations
    </Title>
    <Section>
      Discover the best jobs near you, search by cities and find jobs by location.
    </Section>
  </View>
)

export default IntroLocationsScreen