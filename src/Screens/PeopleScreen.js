// @flow weak

import React from 'react'
import { View } from 'react-native'
import Message from '../Views/Message'
import { shared } from '../Styles'

const PeopleScreen = () => (
  <View style={shared.container}>
    <Message
      style={{marginBottom: 30}}
      primary="People. "
      secondary="Find people, and share your thoughts and experience. 😎"
    />
  </View>
)

export default PeopleScreen