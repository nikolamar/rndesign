// @flow weak

import React, { Component } from 'react'
import { ScrollView, View, Text } from 'react-native'
import Card from '../Views/Card'
import { shared } from '../Styles'
import Nav from '../Views/Nav'
import Grid from '../Views/Grid'
import Title from '../Views/Title'
import Section from '../Views/Section'
import db from '../JSON/db'

const renderItem = item => (
  <Card label={item.category}/>
)

const IntroJobsScreen = () => (
  <View style={shared.container}>
    <Nav rightLabel="Skip"/>
    <Grid
      itemWidth={100}
      itemHeight={100}
      marginVertical={10}
      data={db.categories}
      renderItem={renderItem}
    />
    <Title>
      Jobs
    </Title>
    <Section>
      Discover the best jobs in London and be the first to know with job alerts.
    </Section>
  </View>
)

export default IntroJobsScreen