// @flow weak

import React from 'react'
import { View } from 'react-native'
import Message from '../Views/Message'
import { shared } from '../Styles'

const CompaniesScreen = () => (
  <View style={shared.container}>
    <Message
      style={{marginBottom: 30}}
      primary="Companies. "
      secondary="Discover the best Copmanies in London search companies that are hiring. 😎"
    />
  </View>
)

export default CompaniesScreen