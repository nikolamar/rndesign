// @flow weak

import React from 'react'
import { View } from 'react-native'
import Nav from '../Views/Nav'
import Title from '../Views/Title'
import Section from '../Views/Section'
import { shared, colors } from '../Styles/'
import Button from '../Views/Button'
import { resetAction } from '../Actions'

const IntroEndScreen = ({ navigation }) => (
  <View style={shared.container}>
    <Nav rightLabel="Skip"/>
    <Title>
      Thank You
    </Title>
    <Section style={{marginBottom: 5}}>
      We thank you for your time spent taking this intro. Click on button bellow to go straight to your app.
    </Section>
    <Button
      color={colors.primary}
      style={{marginTop: 20}}
      onPress={() => navigation.dispatch(resetAction("HomeRoute"))}
    >
      Go to App
    </Button>
  </View>
)

export default IntroEndScreen