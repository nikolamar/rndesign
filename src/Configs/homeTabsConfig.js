// @flow weak

import React from 'react'
import { colors } from '../Styles'
import CustomTabs from '../Views/CustomTabs'

 const tabs = ["Jobs", "Companies", "People"]

const homeTabsConfig = {
  swipeEnabled: true,
  animationEnabled: true,
  tabBarComponent: navigation => (
    <CustomTabs
      style={{marginTop: 30}}
      length={navigation.navigationState.routes.length}
      active={navigation.navigationState.index}
      tabs={tabs}
      navigation={navigation}
    />
  ),
  tabBarPosition: "top"
}

export default homeTabsConfig