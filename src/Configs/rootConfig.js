// @flow weak

import { colors } from '../Styles/'

const rootConfig = {
  headerMode: "none",
  cardStyle: {
    backgroundColor: colors.content
  }
}

export default rootConfig