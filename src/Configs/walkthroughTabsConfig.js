// @flow weak

import React from 'react'
import { colors } from '../Styles'
import IntroFooter from '../Views/IntroFooter'

const walkthroughTabsConfig = {
  swipeEnabled: true,
  animationEnabled: true,
  tabBarComponent: ({ navigationState }) => (
    <IntroFooter
      length={navigationState.routes.length}
      active={navigationState.index}
    />
  )
}

export default walkthroughTabsConfig