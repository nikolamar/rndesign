// @flow weak

import { TabNavigator } from 'react-navigation'
import JobsScreen from '../Screens/JobsScreen'
import CompaniesScreen from '../Screens/CompaniesScreen'
import PeopleScreen from '../Screens/PeopleScreen'
import homeTabsConfig from '../Configs/homeTabsConfig'

const HomeRoute = {
  screen: TabNavigator({
    Jobs: {
      screen: JobsScreen,
      navigationOptions: ({ navigation }) => ({
        title: "Jobs",
      })
    },
    Companies: {
      screen: CompaniesScreen,
      navigationOptions: ({ navigation }) => ({
        title: "Companies",
      })
    },
    People: {
      screen: PeopleScreen,
      navigationOptions: ({ navigation }) => ({
        title: "People",
      })
    }
  }, homeTabsConfig)
}

export default HomeRoute