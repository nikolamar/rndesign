// @flow weak

import { TabNavigator } from 'react-navigation'
import IntroScreen from '../Screens/IntroScreen'
import IntroJobsScreen from '../Screens/IntroJobsScreen'
import IntroLocationsScreen from '../Screens/IntroLocationsScreen'
import IntroCompaniesScreen from '../Screens/IntroCompaniesScreen'
import IntroEndScreen from '../Screens/IntroEndScreen'
import walkthroughTabsConfig from '../Configs/walkthroughTabsConfig'

const WalkthroughRoute = { 
  screen: TabNavigator({
    Intro: {
      screen: IntroScreen,
      navigationOptions: ({ navigation }) => ({
        title: "Intro",
      })
    },
    Jobs: {
      screen: IntroJobsScreen,
      navigationOptions: ({ navigation }) => ({
        title: "Jobs",
      })
    },
    Locations: {
      screen: IntroLocationsScreen,
      navigationOptions: ({ navigation }) => ({
        title: "Locations",
      })
    },
    Companies: {
      screen: IntroCompaniesScreen,
      navigationOptions: ({ navigation }) => ({
        title: "Companies",
      })
    },
    IntroEnd: {
      screen: IntroEndScreen,
      navigationOptions: ({ navigation }) => ({
        title: "IntroEnd",
      })
    }
  }, walkthroughTabsConfig)
}

export default WalkthroughRoute