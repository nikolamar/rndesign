// @flow weak

import { View } from 'react-native'
import JobScreen from '../Screens/JobScreen'

const JobRoute = {
  screen: JobScreen 
}

export default JobRoute