// @flow weak

export const colors = {
  primary: "#E75057",
  primary2: "#00A1FF",
  primary3: "#FFB600",
  primary4: "#E75057",
  primary5: "#47CB65",
  primary6: "#113A52",
  primary7: "#27C9D8",
  secondary: "#686868",
  secondary2: "#D8D8D8",
  secondary3: "#555555",
  title: "#FFFFFF",
  content: "#FFFFFF",
}

export const shared = {
  container: {
    flex: 1,
    marginTop: 30,
    paddingHorizontal: 20,
    backgroundColor: colors.content
  },
  bold: {
    fontWeight: "600"
  },
  regular: {
    fontWeight: "400"
  },
  image: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    height: undefined,
    width: undefined,
    backgroundColor: "transparent"
  },
  imageTitle: {
    flex: 1,
    position: "absolute",
    color: colors.title,
    fontSize: 30,
    backgroundColor: "transparent"
  }
}