// @flow weak

import { StackNavigator } from 'react-navigation'
import JobRoute from './Routes/JobRoute'
import HomeRoute from './Routes/HomeRoute'
import WalkthroughRoute from './Routes/WalkthroughRoute'
import rootConfig from './Configs/rootConfig'

const RootNavigator = StackNavigator({
  WalkthroughRoute,
  HomeRoute,
  JobRoute
}, rootConfig)

export default RootNavigator