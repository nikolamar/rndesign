// @flow weak

import React from 'react'
import { TouchableOpacity, Text } from 'react-native'
import { colors } from '../Styles/'

const button = {
  container: {
    height: 40,
    borderRadius: 3,
    borderWidth: 1,
    alignItems: "center",
    justifyContent: "center"
  }
}

const Button = ({ onPress, inverted, children, style, color }) => (
  <TouchableOpacity onPress={onPress} style={{
    ...button.container,
    borderColor: color || "black",
    backgroundColor: inverted ? "transparent" : color,
    ...style
  }}>
    <Text style={{color: inverted ? color || "black" : colors.content}}>{children}</Text>
  </TouchableOpacity>
)

export default Button