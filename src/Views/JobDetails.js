// @flow weak

import React from 'react'
import { View } from 'react-native'
import ListItem from '../Views/ListItem'
import { MaterialIcons } from '@expo/vector-icons'
import { shared, colors } from '../Styles/'

const JobDetails = ({ style, category, pay, required, skills }) => (
  <View style={style}>
    <ListItem
      subtitle={category}
      leftAvatar={
        <MaterialIcons
          name="loyalty"
          size={38}
          color={colors.secondary2}
        />
      }
    />
    <ListItem
      subtitle={pay}
      leftAvatar={
        <MaterialIcons
          name="slow-motion-video"
          size={38}
          color={colors.secondary2}
        />
      }
    />
    <ListItem
      subtitle={required}
      leftAvatar={
        <MaterialIcons
          name="account-balance-wallet"
          size={38}
          color={colors.secondary2}
        />
      }
    />
    <ListItem
      subtitle={skills}
      leftAvatar={
        <MaterialIcons
          name="assessment"
          size={38}
          color={colors.secondary2}
        />
      }
    />
  </View>
)

export default JobDetails