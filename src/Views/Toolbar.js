// @flow weak

import React from 'react'
import { TouchableOpacity, View, Text } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'
import Line from '../Views/Line'
import { shared } from '../Styles'
import { resetAction } from '../Actions'

const Toolbar = ({ navigation }) => (
  <View>
    <View style={{flexDirection: "row", alignItems: "center", height: 60}}>
      <MaterialIcons
        name="keyboard-arrow-left"
        size={35}
      />
      <TouchableOpacity
        style={{flex: 1}}
        onPress={() => navigation.dispatch(resetAction("HomeRoute"))}
      >
        <Text style={shared.bold}>Parent Title</Text>
      </TouchableOpacity>
      <MaterialIcons
        name="share"
        size={25}
      />
      <MaterialIcons
        style={{margin: 10}}
        name="favorite-border"
        size={25}
      />
    </View>
    <Line/>
  </View>
)

export default Toolbar