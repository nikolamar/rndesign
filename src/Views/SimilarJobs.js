// @flow weak

import React from 'react'
import { View, Text } from 'react-native'
import Grid from '../Views/Grid'
import DescriptiveCard from '../Views/DescriptiveCard'
import { colors } from '../Styles/'
import db from '../JSON/db'

const renderSimilarJob = item => (
  <DescriptiveCard
    textContainer={{alignItems: "center"}}
    title={item.title}
    titleColor={colors.primary2}
    subtitle={item.company}
  />
)

const SimilarJobs = ({ style }) => (
  <View style={style}>
    <Text style={{fontWeight: "600", fontSize: 15}}>Similar Jobs</Text>
    <Grid
      horizontal
      itemWidth={145}
      itemHeight={169}
      data={db.similarJobs}
      renderItem={renderSimilarJob}
    />
  </View>
)

export default SimilarJobs