// @flow weak

import React, { Component } from 'react'
import { ScrollView, View } from 'react-native'

const grid = {
  flexWrap: "wrap",
  flexDirection: "row",
  justifyContent: "space-between"
}

/**
 * Grid view is a component that fix 
 * last row items position because
 * of "justifyContent: "space-between"
 * last row items are spread, adding 
 * empty views will do the trick
 * and move items to the left
 * 
 * Also component can be configured
 * to show items horizontally
 * @constructor
 * @param {boolean} horizontal - Grid horizontal orientation
 * @param {function} renderItem - This prop will render your custom item
 * @param {array} data - Data that will be rendered
 * @param {number} itemWidth - Item width
 * @param {number} itemHeight - Item height
 * @param {number} margin - Margin for items
 * @param {number} marginVertical - Vertical margin for items
 */
class GridView extends Component {
  state = {
    width: 0
  }
  renderItem = (item, i) => (
    <View
      key={i}
      style={{
        width: this.props.itemWidth,
        height: this.props.itemHeight,
        margin: this.props.margin,
        marginVertical: this.props.marginVertical
      }}
    >
      {this.props.renderItem(item, i)}
    </View>
  )
  renderEmptyViews = () => {
    if (!this.state.width) return
    let emptyViews = []
    const itemsInRow = Math.floor(this.state.width / this.props.itemWidth)
    const remainder = (this.props.data.length + 1) % itemsInRow
    for (let i = 0; i <= remainder; i++) {
      emptyViews.push(
        <View
          key={i}
          style={{
            width: this.props.itemWidth,
            height: this.props.itemHeight,
            margin: this.props.margin,
            marginVertical: this.props.marginVertical
          }}
        />
      )
    }
    return emptyViews
  }
  onLayout = event => {
    this.setState({ width: event.nativeEvent.layout.width })
  }
  render() {
    return (
      <ScrollView
        horizontal={this.props.horizontal}
        contentContainerStyle={{...grid, ...this.props.style}}
        onLayout={this.onLayout}
      >
        {this.props.data.map((item, i) => this.renderItem(item, i))}
        {!this.props.horizontal ? this.renderEmptyViews() : null}
      </ScrollView>
    )
  }
}

export default GridView