// @flow weak

import React from 'react'
import { View } from 'react-native'
import ListItem from '../Views/ListItem'
import { colors } from '../Styles/'

const RecentlyAdded = ({ data }) => (
  <View>
    {data.map((item, i) => (
      <ListItem
        key={i}
        rightArrow
        title={item.title}
        subtitle={item.subtitle}
        leftAvatar={
          <View style={{
            width: 38,
            height: 38,
            borderRadius: 3,
            backgroundColor: colors.secondary2
          }}/>
        }
      />
    ))}
  </View>
)

export default RecentlyAdded