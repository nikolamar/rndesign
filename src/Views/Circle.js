// @flow weak

import React from 'react'
import { View } from 'react-native'
import { colors } from '../Styles/'

const circle = {
  width: 8,
  height: 8,
  marginLeft: 5,
  borderRadius: 8,
  backgroundColor: colors.primary
}

const Circle = ({ active }) => (
  <View style={{ ...circle, opacity: active ? 1 : 0.3 }}/>
)

export default Circle