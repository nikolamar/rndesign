// @flow weak

import React from 'react'
import { TouchableOpacity, View, TextInput } from 'react-native'
import { Ionicons } from '@expo/vector-icons'
import { shared, colors } from '../Styles/'

const input = {
  container: {
    flexDirection: "row"
  },
  field: {
    flex: 1,
    height: 40,
    fontSize: 12,
    paddingHorizontal: 10,
    borderRightWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderTopRightRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.secondary2
  },
  icon: {
    width: 45,
    height: 40,
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderBottomLeftRadius: 3,
    alignItems: "center",
    justifyContent: "center",
    borderColor: colors.secondary2
  }
}

const Input = ({ style, onTouchIcon, onChange, value, placeholder }) => (
  <View style={{...input.container, ...style}}>
    <TouchableOpacity style={input.icon} onTouch={onTouchIcon}>
      <Ionicons
        name="ios-search"
        size={30}
        color={colors.secondary2}
      />
    </TouchableOpacity>
    <TextInput
      placeholder={placeholder}
      style={input.field}
      onChangeText={onChange}
      value={value}
    />
  </View>
)

export default Input