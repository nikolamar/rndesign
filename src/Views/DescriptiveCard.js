// @flow weak

import React from 'react'
import { colors } from '../Styles/'
import { TouchableOpacity, View, Text } from 'react-native'

const style = {
  card: {
    flexDirection: "row",
    justifyContent: "flex-end",
    backgroundColor: colors.secondary2,
    borderRadius: 6
  },
  cardHeaderContainer: {
    margin: 10,
    height: 25,
    paddingHorizontal: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.content,
    borderRadius: 10,
    opacity: 0.8
  },
  cardHeader: {
    margin: 5,
    fontSize: 12,
    fontFamily: "AvenirNext-Regular",
    fontWeight: "600"
  },
  cardTitle: {
    marginTop: 7,
    fontSize: 12,
    fontFamily: "AvenirNext-Regular",
    fontWeight: "600",
    color: colors.primary2
  },
  cardSubtitle: {
    marginTop: 3,
    fontSize: 15,
    fontFamily: "AvenirNext-Regular",
    fontWeight: "500",
    color: colors.secondary
  }
}

const Head = ({ head, itemCardHeight }) => (
  <View style={style.cardHeaderContainer}>
    <Text numberOfLines={1} style={style.cardHeader}>{head}</Text>
  </View>
)

const DescriptiveCard = ({ textContainer, title, subtitle, head, itemCardWidth = 125, itemCardHeight = 125, titleColor, onPress }) => (
  <TouchableOpacity onPress={onPress} style={{width: itemCardWidth}}>
    <View style={{ ...style.card, height: itemCardHeight }}>
      {head ? <Head head={head} itemCardHeight={itemCardHeight}/> : null}
    </View>
    <View style={textContainer}>
      <Text style={{ ...style.cardTitle, color: titleColor }}>{title.toUpperCase()}</Text>
      <Text style={style.cardSubtitle}>{subtitle}</Text>
    </View>
  </TouchableOpacity>
)

export default DescriptiveCard