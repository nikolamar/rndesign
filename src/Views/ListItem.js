// @flow weak

import React from 'react'
import { TouchableOpacity, View, Text } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'
import Line from '../Views/Line'
import { colors } from '../Styles/'

const listItem = {
  container: {
    flexDirection: "row",
    height: 70,
    alignItems: "center",
    justifyContent: "space-between"
  },
  avatar: {
    marginRight: 20,
    width: 38,
    height: 38,
  },
  title: {
    fontSize: 12,
    fontFamily: "AvenirNext-Regular",
    fontWeight: "600"
  },
  subtitle: {
    fontSize: 15,
    fontFamily: "AvenirNext-Regular",
  }
}

const RightArrow = () => (
  <MaterialIcons
    name="keyboard-arrow-right"
    size={35}
    color={colors.secondary2}
  />
)

const Title = ({ title }) => (
  <Text style={listItem.title}>{title}</Text>
)

const Subtitle = ({ subtitle }) => (
  <Text style={listItem.subtitle}>{subtitle}</Text>
)

const ListItem = ({ leftAvatar, rightArrow, title, subtitle, onTouch }) => (
  <TouchableOpacity onTouch={onTouch}>
    <View style={listItem.container}>
      <View style={listItem.container}>
        <View style={listItem.avatar}>
          {leftAvatar}
        </View>
        <View>
          {title ? <Title title={title}/> : null}
          {subtitle ? <Subtitle subtitle={subtitle}/> : null}
        </View>
      </View>
      {rightArrow ? <RightArrow/> : null}
    </View>
    <Line/>
  </TouchableOpacity>
)

export default ListItem
