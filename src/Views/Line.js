// @flow weak

import React from 'react'
import { View } from 'react-native'
import { colors } from '../Styles/'

const lineStyle = {
  height: 1,
  backgroundColor: colors.secondary2,
}

/**
 * Line is simple view with 1px tickness
 * @constructor
 * @param {object} style - Additional style can by apply
 */
const Line = ({ style }) => (
  <View style={{ ...lineStyle, ...style }}/>
)

export default Line