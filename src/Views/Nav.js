// @flow weak

import React from 'react'
import { TouchableOpacity, View, Text } from 'react-native'
import { colors } from '../Styles/'

const nav = {
  container: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  label: {
    color: colors.secondary,
    fontSize: 15,
    fontFamily: "AvenirNext-Regular"
  },
  leftLabel: {
    textAlign: "left",
  },
  rightLabel: {
    textAlign: "right",
  }
}

/**
 * Nav is text with default styling for this app
 * @constructor
 * @param {string} leftLabel - Label is a text
 * @param {string} rightLabel - Label is a text
 * @param {object} leftStyle - Additional style can by apply
 * @param {object} rightStyle - Additional style can by apply
 */
const Nav = ({ leftLabel, rightLabel, style, leftStyle, rightStyle, onLeftPress, onRightPress }) => (
  <View style={{...nav.container, ...style}}>
    <TouchableOpacity onPress={onLeftPress}>
      <Text style={{ ...nav.label, ...leftStyle }}>{leftLabel}</Text>
    </TouchableOpacity>
    <TouchableOpacity onPress={onRightPress}>
      <Text style={{ ...nav.label, ...rightStyle }}>{rightLabel}</Text>
    </TouchableOpacity>
  </View>
)

export default Nav