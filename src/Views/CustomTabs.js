// @flow weak

import React from 'react'
import { TouchableOpacity, View, Text } from 'react-native'
import Input from '../Views/Input'
import Line from '../Views/Line'
import { colors } from '../Styles/'

const home = {
  tabContainer: {
    flexDirection: "row",
  },
  labelContainer: {
    width: "100%",
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  }
}

const CustomTabs = ({ tabs, length, active = 0, style, navigation }) => {
  let tabsView = []
  for (let i = 0; i < length; i++) {
    const selected = i === active
    tabsView.push(
      <TouchableOpacity key={i} style={{flex: 1}} onPress={() => navigation.jumpToIndex(i)}>
        <View style={home.labelContainer}>
          <Text style={{
            opacity: selected ? 1 : 0.7,
            color: selected ? colors.primary : colors.secondary3
          }}>
            {tabs[i].toUpperCase()}
          </Text>
        </View>
        <Line style={{
          height: selected ? 2 : 1,
          backgroundColor: selected ? colors.primary : colors.secondary,
          opacity: selected ? 1 : 0.3 
        }}/>
      </TouchableOpacity>
    )
  }
  return (
    <View style={style}>
      <Input
        style={{marginHorizontal: 20}}
        placeholder="SEARCH JOBS"
      />
      <View style={home.tabContainer}>
        {tabsView}
      </View>
    </View>
  )
}

export default CustomTabs