// @flow weak

import React from 'react'
import { View, Text } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'
import { MapView } from 'expo'
import { colors } from '../Styles/'

const job = {
  address: {
    flex: 1,
    fontSize: 15,
    color: colors.title
  }
}

const JobLocation = ({ region, style, mapStyle, address }) => (
  <View style={style}>
    <View style={{flexDirection: "row", paddingHorizontal: 20, height: 55, backgroundColor: colors.primary5, alignItems: "center"}}>
      <Text style={job.address}>{address}</Text>
      <MaterialIcons
        name="keyboard-arrow-right"
        size={35}
        color={colors.content}
      />
    </View>
    <MapView
      style={mapStyle}
      initialRegion={region}
    />
  </View>
)

export default JobLocation