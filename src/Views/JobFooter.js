// @flow weak

import React from 'react'
import { View, Text } from 'react-native'
import Button from '../Views/Button'
import { colors } from '../Styles/'

const job = {
  container: {
    height: 181,
    backgroundColor: colors.primary6,
    padding: 20
  },
  text: {
    color: colors.title,
    fontSize: 15
  },
  button: {
    alignSelf: "flex-end",
    width: 195,
    marginTop: 20
  }
}

const JobFooter = () => (
  <View style={job.container}>
    <Text style={job.text}>
      Want to know more about this company? Check out their company page for more jobs, company updates news and more.
    </Text>
    <Button
      color={colors.content}
      inverted
      style={job.button}
    >
      View Company Page
    </Button>
  </View>
)

export default JobFooter