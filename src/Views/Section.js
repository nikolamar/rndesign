// @flow weak

import React from 'react'
import { Text } from 'react-native'
import { colors } from '../Styles/'

const section = {
  marginVertical: 50,
  fontSize: 15,
  fontFamily: "AvenirNext-Regular",
  color: colors.secondary,
}

const Section = ({ children, style }) => (
  <Text style={{ ...section, ...style }}>{children}</Text>
)

export default Section