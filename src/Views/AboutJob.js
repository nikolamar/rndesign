// @flow weak

import React from 'react'
import { View, Text } from 'react-native'
import Button from '../Views/Button'
import { shared, colors } from '../Styles/'

const AboutJob = ({ style }) => (
  <View style={style}>
    <View style={{flexDirection: "row", marginVertical: 50}}>
      <View style={{width: 100, height: 100, backgroundColor: colors.secondary2, borderRadius: 10}}/>
      <View style={{flex: 1, marginLeft: 20}}>
        <Text style={{...shared.bold, fontSize: 18, marginBottom: 5}}>Job Title</Text>
        <Text style={{fontSize: 15}}>Company Name</Text>
        <Button
          inverted
          color="black"
          style={{width: 120, marginTop: 15}}
        >
          Apply
        </Button>
      </View>
    </View>
    <Text style={{...shared.bold, marginBottom: 10, fontSize: 15}}>About</Text>
    <Text style={{fontSize: 15}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ornare imperdiet bibendum… <Text style={{color: colors.primary2}}>see more</Text></Text>
  </View>
)

export default AboutJob