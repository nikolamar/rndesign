// @flow weak

import React from 'react'
import { Text } from 'react-native'

const title = {
  marginTop: 50,
  fontSize: 30,
  fontFamily: "AvenirNext-Bold",
}

const Title = ({ children }) => (
  <Text style={title}>{children}</Text>
)

export default Title