// @flow weak

import React from 'react'
import { Text } from 'react-native'
import { shared, colors } from '../Styles/'

const Message = ({ style, primary, secondary }) => (
  <Text style={{...shared.bold, ...style}}>{primary}<Text style={{...shared.regular, color: colors.secondary}}>{secondary}</Text></Text>
)

export default Message