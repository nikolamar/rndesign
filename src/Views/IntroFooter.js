// @flow weak

import React from 'react'
import { View } from 'react-native'
import Circle from '../Views/Circle'
import Line from '../Views/Line'
import { colors } from '../Styles/'

const footer = {
  container: {
    paddingHorizontal: 20,
    backgroundColor: colors.content
  },
  circles: {
    flexDirection: "row",
    alignItems: "center",
    height: 55
  }
}

const createCircles = (length, active) => {
  let view = []
  for (let i = 0; i < length; i++) {
    view.push(<Circle key={i} active={i === active ? true : false}/>)
  }
  return view
}

const IntroFooter = ({ length = 0, active = 0 }) => (
  <View style={footer.container}>
    <Line/>
    <View style={footer.circles}>
      {createCircles(length, active)}
    </View>
  </View>
)

export default IntroFooter