// @flow weak

import React from 'react'
import { colors } from '../Styles/'
import { TouchableOpacity, View, Text } from 'react-native'

const card = {
  container: {
    backgroundColor: colors.secondary2,
    borderRadius: 6
  },
  label: {
    margin: 5,
    fontSize: 12,
    fontFamily: "AvenirNext-Regular",
    fontWeight: "500",
    textAlign: "center"
  },
  content: {
    flex: 1
  },
  labelContainer: {
    height: 25,
    backgroundColor: colors.content,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    opacity: 0.8
  }
}

const Card = ({ label, onPress, width = 100, height = 100 }) => label ? (
  <TouchableOpacity onPress={onPress} style={{ ...card.container, width, height }}>
    <View style={card.content}/>
    <View style={card.labelContainer}>
      <Text numberOfLines={1} style={card.label}>{label.toUpperCase()}</Text>
    </View>
  </TouchableOpacity>
) : null

export default Card