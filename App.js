// @flow weak

import React, { Component } from 'react'
import RootNavigator from './src/RootNavigator'
import { Font } from 'expo'

class App extends Component {
  state = {
    fontLoaded: false
  }
  async componentDidMount() {
    try {
      await Font.loadAsync({
        avenirNextBold: require("./assets/fonts/avenir-next-bold.ttf"),
        avenirNextItalic: require("./assets/fonts/avenir-next-italic.ttf"),
        avenirNextRegular: require("./assets/fonts/avenir-next-regular.ttf"),
        avenirNextThin: require("./assets/fonts/avenir-next-thin.ttf")
      })
    } catch(error) {
      console.log(error)
    }
    this.setState({ fontLoaded: true })
  }
  render() {
    return (
      <RootNavigator/>
    )
  }
}

export default App
